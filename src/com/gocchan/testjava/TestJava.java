package com.gocchan.testjava;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestJava {

	public static void main(String[] args) {

		int to;
		int now=1;

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

    	System.out.println("エレベーターを開始します。");
    	System.out.println("現在" + now + "階にいます。");
        while(true) {

	        System.out.println("行き先階を入力してください。");

	        String str = null;
	        try {
	            str = br.readLine();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        if(str.equals("stop")) {
	        	System.out.println("エレベーターを終了します。");
	        	break;
	        }
	        try {
	        	Integer.parseInt(str);
	        	to = Integer.parseInt(str);


	        } catch (NumberFormatException e) {
	            e.printStackTrace();
	            continue;
	        }
	        if(to == now) {
	        	System.out.println("すでに" + to + "階にいます。");
	        	continue;
	        } else if(to < 1) {
	        	System.out.println("地下はありません。");
	        	continue;
	        } else if(to > 9) {
	        	System.out.println("この建物は9階建てです。");
	        	continue;
	        }



	        if(now<to) {
		   		for(int i=now+1; i<=to; i++) {
		   			try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
						return;
					}
		   			if(i == to) {
		   				now = i;
		   			} else {
		   				System.out.println(i + "階です。");
		   			}
				}
	        }else {
		   		for(int i=now-1; i>=to; i--) {
		   			try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
						return;
					}
		   			if(i == to) {
		   				now = i;
		   			} else {
		   				System.out.println(i + "階です。");
		   			}
				}

	        }
	        java.awt.Toolkit.getDefaultToolkit().beep();
	        System.out.println(now + "階に到着しました。");

        }
	}
}
